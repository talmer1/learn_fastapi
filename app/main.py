from fastapi import FastAPI
 
app = FastAPI()
 
@app.get("/") #Задаем декоратор для метода GET для пути root
async def root(): #Объявляем функцию root
    return {"message": "Hello World"} #Возвращаем в ответ json при выполнении запроса к http://127.0.0.1:5080/