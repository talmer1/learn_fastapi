FROM python:3.7

WORKDIR /app

RUN pip install fastapi uvicorn psycopg2 py-bcrypt python-multipart PyJWT

EXPOSE 5080

COPY . /app
